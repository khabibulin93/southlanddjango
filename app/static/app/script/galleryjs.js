var LargImg = document.getElementById('LargImg');

document.getElementById('thumbgallery').onclick = function(e) {
  var target = e.target;

  while (target != this) {

    if (target.nodeName == 'A') {
      showThumbnail(target.href, target.title);
      return false;
    }

    target = target.parentNode;
  }

}

function showThumbnail(href, title) {
  LargImg.src = href;
  LargImg.alt = title;
}

var imgs = thumbgallery.getElementsByTagName('img');
for (var i = 0; i < imgs.length; i++) {
  var url = imgs[i].parentNode.href;

  var img = document.createElement('img');
  img.src = url;
}