from django.db import models
from django.utils.text import slugify

# Create your models here.
class SubSection(models.Model):
    #Поразделы секций
    name = models.CharField(verbose_name=u'Название подсекции', max_length=200, default=None)
    section = models.ForeignKey('Section', on_delete=models.CASCADE, related_name='subsection')
    slug = models.CharField(verbose_name=u'Ссылочное имя', max_length=200, editable=False, default=None)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = slugify(self.name, allow_unicode=True)
        super(SubSection, self).save(*args, **kwargs)

    class Meta:
        verbose_name=u'Подсекции'
        verbose_name_plural=u'Подсекции'

class Section(models.Model):
    #Секции недвижмости
    name = models.CharField(verbose_name=u'Название секций', max_length=200)
    slug = models.CharField(verbose_name=u'Ссылочное имя', max_length=200, editable=False, default=None)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = slugify(self.name, allow_unicode=True)
        super(Section, self).save(*args, **kwargs)

    class Meta:
        verbose_name=u'Секция'
        verbose_name_plural=u'Секция'

def custom_directory(instance, filename):
    return 'app/images/{0}/{1}/{2}'.format(instance.property.section.slug, instance.property.slug, filename)

class PropertyPhoto(models.Model):
    photo = models.ImageField(upload_to=custom_directory, default='media/app/images/None/no_image.png', max_length=200)
    property = models.ForeignKey('Property', on_delete=models.CASCADE)


class Property(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    section = models.ForeignKey('Section', on_delete=models.CASCADE, related_name='section_property')
    sub = models.ForeignKey('SubSection', on_delete=models.CASCADE, related_name='sub_property', blank=True, null=True)
    slug = models.CharField(verbose_name=u'Ссылочное имя', max_length=200, editable=False, default=None)
    name = models.CharField(verbose_name=u'Краткое описание', max_length=200)
    description = models.TextField(verbose_name=u'Полное описание', default='Информация не предоставлена')
    cadastre = models.CharField(verbose_name=u'Кадастровый номер', max_length=200, default='Информация не предоставлена', unique=True)
    yardage = models.CharField(verbose_name=u'Площадь', max_length=200, default='Информация не предоставлена')
    price = models.CharField(verbose_name=u'Цена', max_length=200)
    avito_url = models.URLField(verbose_name=u'Ссылка на авито', max_length=200, blank=True)
    PropertyPhoto = None

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = slugify(self.name, allow_unicode=True)
        super(Property, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)
        verbose_name=u'Недвижимость'
        verbose_name_plural=u'Недвижимость'