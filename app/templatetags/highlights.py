from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def active (context, category_name='None', sub_name='None'):
	request = context['request']
	if category_name in request.path or sub_name in request.path:
		return 'active'
	else:
		return 'inactive'