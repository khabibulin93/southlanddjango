from django import template
from app.models import Property, Section, SubSection

register = template.Library()

@register.simple_tag(takes_context=True)
def counter(context, name):
	sub = SubSection.objects.all()
	category = Section.objects.all()

	if name in category:
		property_category = Property.objects.filter(section=name)
		counter_category = 0
		for categorys in property_category:
			counter_category += 1
		return counter_category
	elif name in sub:
		property_sub = Property.objects.filter(sub=name)
		counter_sub = 0
		for subs in property_sub:
			counter_sub += 1
		return counter_sub
	else:
		return '0'
