from django.contrib import admin
from app.models import *
# Register your models here.

class PropertyImageInline(admin.TabularInline):
	model = PropertyPhoto
	fields = ['photo',]

class Sub_Admin(admin.ModelAdmin):
	"""docstring for Country_Admin"""
	list_display = ('name', 'section', )
	list_filter = ('section', )

class Section_Admin(admin.ModelAdmin):
    list_display = ('name', )

class Property_Admin(admin.ModelAdmin):
	fields = (('section', 'sub'), 'name', 'description', 'cadastre', 'yardage', 'price', 'avito_url')
	#radio_fields = {'section':admin.VERTICAL, 'sub': admin.VERTICAL}
	raw_id_fields = ('sub',)
	list_display = ('name', )
	inlines = [PropertyImageInline,]

admin.site.register(SubSection, Sub_Admin)
admin.site.register(Section, Section_Admin)
admin.site.register(Property, Property_Admin)