# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-01-20 11:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20180105_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='description',
            field=models.TextField(blank=True, default=None, verbose_name='Полное описание'),
        ),
    ]
