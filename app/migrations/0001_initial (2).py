# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-15 11:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=200, verbose_name='Краткое описание')),
                ('description', models.TextField(verbose_name='Полное описание')),
                ('cadastre', models.CharField(max_length=200, unique=True, verbose_name='Кадастровый номер')),
                ('yardage', models.CharField(max_length=200, verbose_name='Площадь')),
                ('price', models.CharField(max_length=200, verbose_name='Цена')),
            ],
            options={
                'verbose_name': 'Недвижимость',
                'ordering': ('created',),
                'verbose_name_plural': 'Недвижимость',
            },
        ),
        migrations.CreateModel(
            name='PropertyPhoto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='static/app/images/')),
                ('property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Property')),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Название секций')),
            ],
            options={
                'verbose_name': 'Секция',
                'verbose_name_plural': 'Секция',
            },
        ),
        migrations.CreateModel(
            name='Sub_Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=None, max_length=200, verbose_name='Название города')),
                ('section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sub_section', to='app.Section')),
            ],
            options={
                'verbose_name': 'Подсекции',
                'verbose_name_plural': 'Подсекции',
            },
        ),
        migrations.AddField(
            model_name='property',
            name='section',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='section_property', to='app.Section'),
        ),
        migrations.AddField(
            model_name='property',
            name='sub',
            field=models.ForeignKey(blank=True, default=None, on_delete=django.db.models.deletion.CASCADE, related_name='sub_property', to='app.Sub_Section'),
        ),
    ]
