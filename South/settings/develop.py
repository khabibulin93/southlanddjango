from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

MEDIA_ROOT = '/home/alexey/Dropbox/Workspace/SouthDjango/'

STATIC_ROOT = [
    os.path.join(BASE_DIR, "static"),
    '/static/',
]

STATIC_ROOT = '/home/alexey/Dropbox/Workspace/SouthDjango/static/'