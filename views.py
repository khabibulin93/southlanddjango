from django.shortcuts import render
from datetime import datetime
from app.models import Property, Section, PropertyPhoto, SubSection


# Create your views here.
def home(request):
	property = Property.objects.all()
	categorys = Section.objects.all()
	return render(
		request,
		'app/index.html',
		{
		'year':datetime.now().year,
		'property': property,
		'categorys': categorys
		}
		)

def category(request, category_name):
	property = Property.objects.filter(section__slug=category_name)
	categorys = Section.objects.all()
	return render(
		request,
		'app/index.html',
		{
		'year':datetime.now().year,
		'property': property,
		'categorys': categorys,
		'active_category': True
		}
		)
"""def sub_category(request, category_name, sub_name):
	property = Property.objects.filter(section__slug=category_name, sub__slug=sub_name)
	sub_categorys = SubSection.objects.filter(section__slug=category_name)
	categorys = Section.objects.all()
	return render(
		request,
		'app/index.html',
		{
		'year':datetime.now().year,
		'property': property,
		'categorys': categorys,
		'sub_categorys': sub_categorys,
		'active_sub_category': True
		}
		)
	"""
def object_detail(request, category_name, proper_name):
	property = Property.objects.filter(slug=proper_name)
	categorys = Section.objects.all()
	photo = PropertyPhoto.objects.filter(property__slug=proper_name)
	return render(
		request,
		'app/objects.html',
		{
		'year':datetime.now().year,
		'property': property,
		'categorys': categorys,
		'photo': photo,
		'active_object': True
		}
		)

def contact (request):
	return render(
		request,
		'app/contact.html',
		{
		'year':datetime.now().year,
		}
		)

def about (request):
	return render(
		request,
		'app/about.html',
		{
		'year':datetime.now().year,
		}
		)